import React from 'react';
import { render } from '@testing-library/react';
import Feed from './Feed';

test('renders learn react link', () => {
  const { getByText } = render(<Feed />);
  const linkElement = getByText(/learn react/i);
});
