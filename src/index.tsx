import React from 'react';
import ReactDOM from 'react-dom';
import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';

import Feed from './Feed'
import BookMarkComment from './BookMarkComment'
import NavBar from './NavBar'

ReactDOM.render(
  <React.StrictMode>
    <Router>
      <NavBar/>
      <Switch>
      <Route path="/comment/:url">
          <BookMarkComment />
        </Route>
        <Route path="/">
          <Feed/>
        </Route>
      </Switch>
    </Router>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
