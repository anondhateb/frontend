import * as jspb from "google-protobuf"

import * as google_protobuf_timestamp_pb from 'google-protobuf/google/protobuf/timestamp_pb';

export class FeedRequest extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): FeedRequest.AsObject;
  static toObject(includeInstance: boolean, msg: FeedRequest): FeedRequest.AsObject;
  static serializeBinaryToWriter(message: FeedRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): FeedRequest;
  static deserializeBinaryFromReader(message: FeedRequest, reader: jspb.BinaryReader): FeedRequest;
}

export namespace FeedRequest {
  export type AsObject = {
  }
}

export class FeedReply extends jspb.Message {
  getItemList(): Array<BookMarkReply>;
  setItemList(value: Array<BookMarkReply>): void;
  clearItemList(): void;
  addItem(value?: BookMarkReply, index?: number): BookMarkReply;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): FeedReply.AsObject;
  static toObject(includeInstance: boolean, msg: FeedReply): FeedReply.AsObject;
  static serializeBinaryToWriter(message: FeedReply, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): FeedReply;
  static deserializeBinaryFromReader(message: FeedReply, reader: jspb.BinaryReader): FeedReply;
}

export namespace FeedReply {
  export type AsObject = {
    itemList: Array<BookMarkReply.AsObject>,
  }
}

export class BookMarkReply extends jspb.Message {
  getTitle(): string;
  setTitle(value: string): void;

  getDate(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setDate(value?: google_protobuf_timestamp_pb.Timestamp): void;
  hasDate(): boolean;
  clearDate(): void;

  getLink(): string;
  setLink(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): BookMarkReply.AsObject;
  static toObject(includeInstance: boolean, msg: BookMarkReply): BookMarkReply.AsObject;
  static serializeBinaryToWriter(message: BookMarkReply, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): BookMarkReply;
  static deserializeBinaryFromReader(message: BookMarkReply, reader: jspb.BinaryReader): BookMarkReply;
}

export namespace BookMarkReply {
  export type AsObject = {
    title: string,
    date?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    link: string,
  }
}

export class BookMarkCommentRequest extends jspb.Message {
  getUrl(): string;
  setUrl(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): BookMarkCommentRequest.AsObject;
  static toObject(includeInstance: boolean, msg: BookMarkCommentRequest): BookMarkCommentRequest.AsObject;
  static serializeBinaryToWriter(message: BookMarkCommentRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): BookMarkCommentRequest;
  static deserializeBinaryFromReader(message: BookMarkCommentRequest, reader: jspb.BinaryReader): BookMarkCommentRequest;
}

export namespace BookMarkCommentRequest {
  export type AsObject = {
    url: string,
  }
}

export class BookMarkCommentReply extends jspb.Message {
  getTitle(): string;
  setTitle(value: string): void;

  getCommentList(): Array<CommentReply>;
  setCommentList(value: Array<CommentReply>): void;
  clearCommentList(): void;
  addComment(value?: CommentReply, index?: number): CommentReply;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): BookMarkCommentReply.AsObject;
  static toObject(includeInstance: boolean, msg: BookMarkCommentReply): BookMarkCommentReply.AsObject;
  static serializeBinaryToWriter(message: BookMarkCommentReply, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): BookMarkCommentReply;
  static deserializeBinaryFromReader(message: BookMarkCommentReply, reader: jspb.BinaryReader): BookMarkCommentReply;
}

export namespace BookMarkCommentReply {
  export type AsObject = {
    title: string,
    commentList: Array<CommentReply.AsObject>,
  }
}

export class CommentReply extends jspb.Message {
  getUser(): string;
  setUser(value: string): void;

  getCommnet(): string;
  setCommnet(value: string): void;

  getTimestamp(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setTimestamp(value?: google_protobuf_timestamp_pb.Timestamp): void;
  hasTimestamp(): boolean;
  clearTimestamp(): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CommentReply.AsObject;
  static toObject(includeInstance: boolean, msg: CommentReply): CommentReply.AsObject;
  static serializeBinaryToWriter(message: CommentReply, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CommentReply;
  static deserializeBinaryFromReader(message: CommentReply, reader: jspb.BinaryReader): CommentReply;
}

export namespace CommentReply {
  export type AsObject = {
    user: string,
    commnet: string,
    timestamp?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

