import * as grpcWeb from 'grpc-web';

import * as google_protobuf_timestamp_pb from 'google-protobuf/google/protobuf/timestamp_pb';

import {
  BookMarkCommentReply,
  BookMarkCommentRequest,
  FeedReply,
  FeedRequest} from './hateb_pb';

export class HatebClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: string; });

  getFeed(
    request: FeedRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.Error,
               response: FeedReply) => void
  ): grpcWeb.ClientReadableStream<FeedReply>;

  getBookMarkComment(
    request: BookMarkCommentRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.Error,
               response: BookMarkCommentReply) => void
  ): grpcWeb.ClientReadableStream<BookMarkCommentReply>;

}

export class HatebPromiseClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: string; });

  getFeed(
    request: FeedRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<FeedReply>;

  getBookMarkComment(
    request: BookMarkCommentRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<BookMarkCommentReply>;

}

