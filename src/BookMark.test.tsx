import React from 'react';
import { render } from '@testing-library/react';
import BookMark from './BookMark';
import {BookMarkReply} from './proto/hateb_pb';

test('renders learn react link', () => {
  const br: BookMarkReply = new BookMarkReply();
  br.setTitle("foo");
  br.setLink("https://example.com");

  const { getByText } = render(<BookMark bookMarkReply={br} />);
  const linkElement = getByText(/learn react/i);
});
