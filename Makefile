.PHONY: proto dev build deploy

PWD = $(shell pwd)
OUT_DIR = ./src/proto
S3_BUCKET_NAME = hbm.programmeboy.com

proto:
	rm ./src/proto/*
	protoc -I protobuf/ protobuf/greeter.proto \
		--js_out=import_style=commonjs:./src/proto
	protoc -I protobuf/ protobuf/hateb.proto \
		--js_out=import_style=commonjs:./src/proto
	protoc -I protobuf/ protobuf/greeter.proto \
		--js_out=import_style=commonjs+dts:./src/proto \
		--grpc-web_out=import_style=commonjs+dts,mode=grpcwebtext:./src/proto
	protoc -I protobuf/ protobuf/hateb.proto \
		--js_out=import_style=commonjs+dts:./src/proto \
		--grpc-web_out=import_style=commonjs+dts,mode=grpcwebtext:./src/proto
	for f in $(OUT_DIR)/*.js; do \
		echo '/* eslint-disable */' | cat - "$${f}" > temp && mv temp "$${f}" ; \
	done

dev:
	npm start

build:
	npm run build

deploy:
	gsutil rm gs://static.anondhateb.com/**
	gsutil rsync -R build gs://static.anondhateb.com

deploy_s3:
	aws s3 cp ./build s3://$(S3_BUCKET_NAME) --recursive
